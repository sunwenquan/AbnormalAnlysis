from random import choice
def get_random_student(filename):
    """
    从文件中随机抽取一个学生，文件格式如下：
    张三
    李四
    ...
    :param filename:文件名，绝对路径
    :return:返回一个字符串
    """
    with open(filename,encoding='UTF8') as f:
        return choice(f.readlines())

if __name__ == '__main__':
    get_random_student('D:\Writing\gitee\sunwenquan\AbnormalAnlysis\AbnormalAnlysis\data\students.txt')

