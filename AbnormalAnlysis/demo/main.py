"""
pyQt5实现随机点名
"""
import os
import sys

from PyQt5.QtWidgets import QMainWindow, QApplication

from demo.students import Ui_MainWindow
from utils.my_random import get_random_student
from conf import settings
class StudentWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(StudentWindow, self).__init__()
        self.setupUi(self)
        self.label.setText('')
        self.pushButton.clicked.connect(self.set_name)
        self.show()
    def set_name(self):
        try:
            name = get_random_student(os.path.join(settings.BASE_DIR,'data','students.txt'))
        except Exception as err:
            print(err)
        self.label.setText(name)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    stu = StudentWindow()

    sys.exit(app.exec_())
