'''pyinstaller --noconfirm --clean --windowed
--specpath=$ProjectFileDir$\build
--workpath=$ProjectFileDir$\build
--distpath=$ProjectFileDir$\bin
--icon=$ProjectFileDir$\res\$FileNameWithoutExtension$.ico
--version-file=$ProjectFileDir$\res\file_version_info.txt
--add-binary=$ProjectFileDir$\venv\Lib\site-packages\cv2\opencv_ffmpeg341_64.dll;./cv2
--paths=$ProjectFileDir$\venv\Lib\site-packages\PyQt5\Qt\bin;$ProjectFileDir$\res;$ProjectFileDir$;$ProjectFileDir$\core;$ProjectFileDir$\core\view;$ProjectFileDir$\core\model;$ProjectFileDir$\core\control;$ProjectFileDir$\core\conf $FileName$
'''
import os

cmd = """pyinstaller --noconfirm --clean --windowed \
--specpath=build --workpath=build --distpath=bin \
--icon=D:/Writing/gitee/sunwenquan/AbnormalAnlysis/AbnormalAnlysis/ico/analysis_128.ico \
--paths=AbnormalAnlysis/venv/Lib/site-packages/PyQt5/Qt/bin;AbnormalAnlysis/data;AbnormalAnlysis/conf \
AbnormalAnlysis/demo/main.py
"""
os.system(cmd)
